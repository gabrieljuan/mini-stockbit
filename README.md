## Mini Stockbit

What can be done with this app

1. Load top cryptocurrency past 24H with load more and pull to refresh features
2. Visualize login screen

Used tech stack

1. Kotlin coroutines with retrofit 2.6.0 and livedata
2. Single activity with navigation (login fragment and watchlist fragment)
3. Koin as dependency injection
4. Unit test including coroutines and livedata
5. multi-module clean architecture showcase
6. Material theme showcase (see login fragment)
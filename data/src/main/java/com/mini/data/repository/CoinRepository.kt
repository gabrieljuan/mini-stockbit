package com.mini.data.repository

import com.mini.data.source.CoinSource
import com.mini.domain.response.CoinResponse
import retrofit2.Response

class CoinRepository(private val coinSource: CoinSource) {

    suspend fun getTopList24H(limit: Int, page: Int, toSymbol: String): Response<CoinResponse> {
        return coinSource.getTopList24H(limit, page, toSymbol)
    }
}
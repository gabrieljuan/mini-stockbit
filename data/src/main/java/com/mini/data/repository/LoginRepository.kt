package com.mini.data.repository

import com.mini.data.source.LoginSource
import com.mini.data.source.LoginTokenSource
import com.mini.domain.LoginRequestBody
import com.mini.domain.UserToken

class LoginRepository(private val loginSource: LoginSource, private val loginTokenSource: LoginTokenSource) {

    fun getUserToken(): UserToken = loginTokenSource.getToken()

    //User can login using token acquired from social media login (google / facebook)
    fun loginWithToken(token: UserToken? = null) {
        token?.let { loginTokenSource.saveToken(it) }

        login(token)
    }

    //User can Login using email / username with its password
    fun loginWithEmail(email: String, password: String){
        val requestBody = LoginRequestBody(email, password)

        login(loginRequestBody = requestBody)
    }

    private fun login(token: UserToken? = null, loginRequestBody: LoginRequestBody? = null) {
        //Try login with token, null return means there is no token
        var loginResponse = loginTokenSource.loginWithToken()

        if (loginResponse == null && loginRequestBody != null) {

        }
    }
}
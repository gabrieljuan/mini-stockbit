package com.mini.data.source

import com.mini.domain.response.CoinResponse
import retrofit2.Response

interface CoinSource {
   suspend fun getTopList24H(limit: Int, page: Int, toSymbol: String): Response<CoinResponse>
}
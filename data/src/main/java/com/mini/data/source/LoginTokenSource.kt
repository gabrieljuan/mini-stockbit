package com.mini.data.source

import com.mini.domain.UserToken

interface LoginTokenSource {

    fun saveToken(token: UserToken)

    fun getToken(): UserToken

    fun loginWithToken()
}
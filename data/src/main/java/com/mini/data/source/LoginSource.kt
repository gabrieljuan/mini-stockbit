package com.mini.data.source

import com.mini.domain.LoginRequestBody

interface LoginSource {
    fun login(loginRequestBody: LoginRequestBody)
}
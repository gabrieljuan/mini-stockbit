package com.mini.domain

data class LoginResponse(val userData: UserData, val userToken: UserToken)
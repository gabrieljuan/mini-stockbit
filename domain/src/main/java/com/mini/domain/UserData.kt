package com.mini.domain

data class UserData(val userId: String, val userName: String)
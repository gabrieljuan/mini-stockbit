package com.mini.domain

data class LoginRequestBody(val email: String, val password:String)
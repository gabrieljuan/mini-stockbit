package com.mini.domain

data class UserToken(val tokenId: String)
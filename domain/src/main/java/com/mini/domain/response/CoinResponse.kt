package com.mini.domain.response

import com.google.gson.annotations.SerializedName

data class CoinResponse(
    @SerializedName("Data")
    val coinData: List<CoinData>
)
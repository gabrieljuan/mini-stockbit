package com.mini.domain.response

import com.google.gson.annotations.SerializedName

class CoinPrice(
    @SerializedName("USD") val coinPriceDetail: CoinPriceDetail
)
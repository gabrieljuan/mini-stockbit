package com.mini.domain.response

import com.google.gson.annotations.SerializedName

data class CoinInfo(
    @SerializedName("Id")
    val coinId: String,
    @SerializedName("Name")
    val coinName: String,
    @SerializedName("FullName")
    val fullName: String,
)
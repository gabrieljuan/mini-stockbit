package com.mini.domain.response

import com.google.gson.annotations.SerializedName

data class CoinPriceDetail(
    @SerializedName("PRICE")
    val currentPrice: Float,
    @SerializedName("CHANGEHOUR")
    val changeFromLastHour: Float
)
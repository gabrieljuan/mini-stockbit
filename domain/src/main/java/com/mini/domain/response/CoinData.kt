package com.mini.domain.response

import com.google.gson.annotations.SerializedName

data class CoinData(
    @SerializedName("CoinInfo")
    val coinInfo: CoinInfo,
    @SerializedName("RAW")
    val coinPrice: CoinPrice
)
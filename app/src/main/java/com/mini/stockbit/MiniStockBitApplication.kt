package com.mini.stockbit

import android.app.Application
import com.mini.stockbit.framework.di.fragmentModule
import com.mini.stockbit.framework.di.networkModule
import com.mini.stockbit.framework.di.repositoryModule
import com.mini.stockbit.framework.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MiniStockBitApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MiniStockBitApplication)
            modules(listOf(fragmentModule, viewModelModule, networkModule,
                repositoryModule
            ))
        }
    }
}
package com.mini.stockbit.framework.di

import com.mini.stockbit.ui.login.LoginFragment
import com.mini.stockbit.ui.watchlist.WatchlistFragment
import org.koin.dsl.module

val fragmentModule = module {
    factory { WatchlistFragment() }
    factory { LoginFragment() }
}
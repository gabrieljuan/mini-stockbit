package com.mini.stockbit.framework.di

import com.mini.stockbit.BuildConfig
import com.mini.stockbit.framework.network.AuthenticationInterceptor
import com.mini.stockbit.framework.network.MiniStockBitAPI
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    factory { AuthenticationInterceptor() }
    factory { provideOkHttpClient(get(), get()) }
    factory { provideMiniStockBitApi(get()) }
    factory { provideLoggingInterceptor() }
    single { provideRetrofit(get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.API_URL).client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(authInterceptor: AuthenticationInterceptor, loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient()
        .newBuilder()
        .addInterceptor(loggingInterceptor)
        .addInterceptor(authInterceptor)
        .build()
}

fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val logger = HttpLoggingInterceptor()
    logger.level = HttpLoggingInterceptor.Level.BASIC
    return logger
}

fun provideMiniStockBitApi(retrofit: Retrofit): MiniStockBitAPI = retrofit.create(MiniStockBitAPI::class.java)

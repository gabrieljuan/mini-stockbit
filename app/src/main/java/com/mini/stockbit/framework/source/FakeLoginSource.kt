package com.mini.stockbit.framework.source

import com.mini.data.source.LoginSource
import com.mini.domain.LoginRequestBody

class FakeLoginSource: LoginSource {

    override fun login(loginRequestBody: LoginRequestBody) {
        //Do login here
    }
}
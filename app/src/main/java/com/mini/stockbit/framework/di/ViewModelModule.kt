package com.mini.stockbit.framework.di

import com.mini.stockbit.ui.login.LoginViewModel
import com.mini.stockbit.ui.watchlist.WatchlistViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { WatchlistViewModel(get()) }
    viewModel { LoginViewModel() }
}
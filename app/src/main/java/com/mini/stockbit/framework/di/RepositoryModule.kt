package com.mini.stockbit.framework.di

import com.mini.data.repository.CoinRepository
import com.mini.data.source.CoinSource
import com.mini.stockbit.framework.source.CoinRemoteSource
import org.koin.dsl.module

val repositoryModule = module {
    factory { CoinRepository(get()) }
    factory<CoinSource> { CoinRemoteSource(get()) }
}
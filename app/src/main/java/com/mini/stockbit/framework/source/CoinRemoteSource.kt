package com.mini.stockbit.framework.source

import com.mini.data.source.CoinSource
import com.mini.domain.response.CoinResponse
import com.mini.stockbit.framework.network.MiniStockBitAPI
import retrofit2.Response

class CoinRemoteSource(private val miniStockBitAPI: MiniStockBitAPI): CoinSource {

    override suspend fun getTopList24H(limit: Int, page: Int, toSymbol: String): Response<CoinResponse> {
        return miniStockBitAPI.getTopList24H(limit.toString(), page.toString(), toSymbol)
    }
}
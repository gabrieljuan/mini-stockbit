package com.mini.stockbit.framework.network

import okhttp3.Interceptor
import okhttp3.Response
import com.mini.stockbit.BuildConfig

class AuthenticationInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var req = chain.request()
        val url = req.url.newBuilder().addQueryParameter("api_key", BuildConfig.API_KEY).build()

        req = req.newBuilder().url(url).build()

        return chain.proceed(req)
    }
}
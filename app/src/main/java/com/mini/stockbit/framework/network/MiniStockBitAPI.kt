package com.mini.stockbit.framework.network

import com.mini.domain.response.CoinResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MiniStockBitAPI {

    @GET("top/totaltoptiervolfull")
    suspend fun getTopList24H(@Query("limit") limit: String,
                              @Query("page") page: String,
                              @Query("tsym") toSymbol: String): Response<CoinResponse>
}
package com.mini.stockbit.ui.watchlist

import android.util.Log
import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.mini.data.repository.CoinRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers

class WatchlistViewModel(private val coinRepository: CoinRepository) : ViewModel() {

    private val currentPage = MutableLiveData<Int>()

    private val MAX_PAGE_LIMIT = 4
    private val ITEMS_PER_PAGE = 10

    var coroutineExceptionHandler: CoroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("coroutine", "coroutine error", throwable)
    }

    var coinList = currentPage.switchMap {
        liveData(Dispatchers.IO + coroutineExceptionHandler) {
            emit(coinRepository.getTopList24H(ITEMS_PER_PAGE, it, "USD"))
        }
    }

    fun loadMore() {
        currentPage.value?.let {
            if (it < MAX_PAGE_LIMIT) {
                currentPage.value = it + 1
            }
        }
    }

    fun initialLoad() {
        currentPage.value = 0
    }

    fun isInitiateLoad() = currentPage.value ?: 0 == 0

    fun isEndOfStream() = currentPage.value ?: 0 == MAX_PAGE_LIMIT
}
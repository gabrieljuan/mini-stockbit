package com.mini.stockbit.ui.coin

import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.mini.domain.response.CoinData
import com.mini.stockbit.R
import com.mini.stockbit.databinding.ItemCoinBinding
import java.text.NumberFormat
import java.util.*

class ItemCoinViewHolder(private val dataBinding: ViewDataBinding) : RecyclerView.ViewHolder(dataBinding.root) {

    fun bindItem(coinData: CoinData) {
        (dataBinding as ItemCoinBinding).apply {
            val priceDetail = coinData.coinPrice.coinPriceDetail

            coinNameText.text = coinData.coinInfo.coinName
            coinFullNameText.text = coinData.coinInfo.fullName
            coinCurrentPriceText.text = formatNumber(priceDetail.currentPrice)
            coinPriceDiffText.text = getPriceChangesString(priceDetail.currentPrice, priceDetail.changeFromLastHour)
            if (priceDetail.changeFromLastHour > 0)
                coinPriceDiffText.setTextColor(ContextCompat.getColor(dataBinding.root.context, R.color.colorAccent))
            else if (priceDetail.changeFromLastHour < 0)
                coinPriceDiffText.setTextColor(Color.RED)
        }
    }

    private fun formatNumber(number: Float): String {
        return NumberFormat.getIntegerInstance(Locale.US).format(number.toDouble())
    }

    private fun formatNumberToPercent(number: Float): String {
        return NumberFormat.getPercentInstance(Locale.US).apply { maximumFractionDigits = 2 }.format(number.toDouble())
    }

    private fun getPriceChangesString(currentPrice: Float, priceChange: Float): String {
        val diffPriceString = formatNumber(priceChange)
        val diffPricePercentString = formatNumberToPercent(priceChange / currentPrice)

        return "$diffPriceString($diffPricePercentString)"
    }
}
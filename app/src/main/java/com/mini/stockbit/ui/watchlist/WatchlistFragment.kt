package com.mini.stockbit.ui.watchlist

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mini.domain.response.CoinResponse
import com.mini.stockbit.R
import com.mini.stockbit.databinding.FragmentWatchlistBinding
import com.mini.stockbit.ui.coin.CoinAdapter
import com.mini.stockbit.ui.general.LinearBottomScreenOnScrollListener
import kotlinx.coroutines.CoroutineExceptionHandler
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.Response

class WatchlistFragment : Fragment() {

    private val watchlistViewModel: WatchlistViewModel by viewModel()
    private lateinit var binding: FragmentWatchlistBinding

    private val coinAdapter = CoinAdapter()
    private val bottomScreenOnScrollListener = LinearBottomScreenOnScrollListener()

    private val coinDataObserver = Observer<Response<CoinResponse>> {
        if (it.isSuccessful) {
            mapResult(it.body())
        } else {
            handleException(it.message())
        }
    }

    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        handleException(throwable.message)
    }

    private val almostReachBottomListener = object : LinearBottomScreenOnScrollListener.AlmostReachBottomListener {
        override fun almostReachBottom() {
            watchlistViewModel.loadMore()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_watchlist, container, false)

        setupRecyclerView()
        setupSwipeRefresh()
        watchlistViewModel.coroutineExceptionHandler = this.coroutineExceptionHandler
        doInitialCoinLoad()

        return binding.root
    }

    private fun doInitialCoinLoad() {
        binding.swipeRefreshLayout.isRefreshing = true
        watchlistViewModel.coinList.observe(viewLifecycleOwner, coinDataObserver)
        watchlistViewModel.initialLoad()
    }

    private fun setupRecyclerView() {
        binding.coinRecyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = coinAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            addOnScrollListener(bottomScreenOnScrollListener.apply {
                setAlmostReachBottomListener(almostReachBottomListener)
            })
        }
    }

    private fun setupSwipeRefresh() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            watchlistViewModel.initialLoad()
        }
    }

    private fun mapResult(coinResponse: CoinResponse?) {
        if (coinResponse?.coinData == null) {
            handleException(null)
            return
        } else {
            if (watchlistViewModel.isInitiateLoad()) {
                coinAdapter.setItems(coinResponse.coinData)
                binding.emptyState.visibility = View.GONE
                binding.swipeRefreshLayout.isRefreshing = false
            } else {
                coinAdapter.addItems(coinResponse.coinData)
            }
            bottomScreenOnScrollListener.setIsLoading(false)
            coinAdapter.setLoadMore(!watchlistViewModel.isEndOfStream())
        }
    }

    private fun handleException(message: String?) {
        binding.swipeRefreshLayout.isRefreshing = false
        if(message != null)
            Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT).setBackgroundTint(Color.RED).show()
        else
            Snackbar.make(binding.root, R.string.default_error_message, Snackbar.LENGTH_SHORT).setBackgroundTint(Color.RED).show()
    }
}
package com.mini.stockbit.ui.coin

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.mini.domain.response.CoinData
import com.mini.stockbit.R
import com.mini.stockbit.databinding.ItemCoinBinding

class CoinAdapter : RecyclerView.Adapter<ItemCoinViewHolder>() {

    private val CONTENT_VIEW = 1
    private val LOAD_MORE_VIEW = 2

    private val items = mutableListOf<CoinData>()
    private var showLoadMore = false

    fun addItems(items: List<CoinData>) {
        this.items.addAll(items)
        notifyItemRangeInserted(this.items.size, items.size)
    }

    fun setItems(items: List<CoinData>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun setLoadMore(isLoadMoreVisible: Boolean) {
        if(showLoadMore == isLoadMoreVisible)
            return

        showLoadMore = isLoadMoreVisible
        if (showLoadMore) {
            notifyItemInserted(items.size)
        } else {
            notifyItemRemoved(items.size)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemCoinViewHolder {
        val viewDataBinding = if (viewType == CONTENT_VIEW)
            DataBindingUtil.inflate<ItemCoinBinding>(LayoutInflater.from(parent.context), R.layout.item_coin, parent, false)
        else
            DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context), R.layout.footer_loading, parent, false)

        return ItemCoinViewHolder(viewDataBinding)
    }

    override fun onBindViewHolder(holder: ItemCoinViewHolder, position: Int) {
        if(getItemViewType(position) == CONTENT_VIEW)
            holder.bindItem(items[position])
    }

    override fun getItemViewType(position: Int): Int =
        if (position == items.size && showLoadMore)
            LOAD_MORE_VIEW
        else
            CONTENT_VIEW

    override fun getItemCount(): Int =
        if (showLoadMore)
            items.size + 1
        else
            items.size

}
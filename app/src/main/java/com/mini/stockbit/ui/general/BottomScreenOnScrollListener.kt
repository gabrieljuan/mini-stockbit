package com.mini.stockbit.ui.general

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class LinearBottomScreenOnScrollListener : RecyclerView.OnScrollListener() {

    private var visibleItemCount = 0
    private var totalItemCount = 0
    private var bottomListener: AlmostReachBottomListener? = null
    private var isLoading = false

    // The minimum amount of items to have below your current scroll position before loading more.
    private val VISIBLE_THRESHOLD_IN_LIST_MODE = 1

    fun setAlmostReachBottomListener(almostReachBottomListener: AlmostReachBottomListener) {
        bottomListener = almostReachBottomListener
    }

    fun setIsLoading(isLoading: Boolean) {
        this.isLoading = isLoading
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        visibleItemCount = recyclerView.childCount
        totalItemCount = recyclerView.layoutManager?.itemCount ?: 0

        val lastCompletelyVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()

//        var firstVisibleItemPosition = 0
//        firstVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

        if (lastCompletelyVisibleItemPosition == totalItemCount - 1 && !isLoading) {
            bottomListener?.almostReachBottom()
            isLoading = true
        }
    }

    interface AlmostReachBottomListener {
        fun almostReachBottom()
    }
}
package com.mini.stockbit.watchlist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.mini.data.repository.CoinRepository
import com.mini.domain.response.CoinResponse
import com.mini.stockbit.testrule.CoroutineTestRule
import com.mini.stockbit.ui.watchlist.WatchlistViewModel
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.OverrideMockKs
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response

@ExperimentalCoroutinesApi
class WatchlistUnitTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = CoroutineTestRule()

    @Before
    fun setup() {
        MockKAnnotations.init(this)
    }

    @MockK
    lateinit var repository: CoinRepository

    @OverrideMockKs
    lateinit var viewModel: WatchlistViewModel

    //set relax unit fun to 'true' so mockk won't return error if we didn't define answers for unit fun that invoked
    @MockK(relaxUnitFun = true)
    lateinit var coinDataObserver: Observer<Response<CoinResponse>>

    @Test
    fun fetchCoin() {
        //setup capture slot to assert params used to fetch coins
        val paramSlot = slot<Int>()
        val param2Slot = slot<Int>()
        val param3Slot = slot<String>()

        //create dummy data and mock to be used as response from Backend
        val coinResponse = CoinResponse(emptyList())
        val responseMock = mockk<Response<CoinResponse>>()

        //give coin response whenever responseMock,body() called
        every { responseMock.body() } returns coinResponse
        //return responseMock whenever repository fetch coin data
        testCoroutineRule.runBlockingTest {
            coEvery { repository.getTopList24H(capture(paramSlot), capture(param2Slot), capture(param3Slot)) } returns responseMock
        }
        //attach observer
        viewModel.coinList.observeForever(coinDataObserver)

        //Trigger the coin fetch
        viewModel.initialLoad()

        coVerify(exactly = 1) { repository.getTopList24H(any(), any(), any()) }
        verify { coinDataObserver.onChanged(responseMock) }
        paramSlot.captured shouldBeEqualTo 10
        param2Slot.captured shouldBeEqualTo 0
        param3Slot.captured shouldBeEqualTo "USD"

        //detach observer
        viewModel.coinList.removeObserver(coinDataObserver)
    }
}